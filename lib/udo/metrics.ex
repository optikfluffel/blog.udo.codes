defmodule Udo.Metrics do
  @moduledoc """
  The Metrics context.
  """
  use Supervisor

  @worker Udo.Metrics.Worker
  @registry Udo.Metrics.Registry
  @supervisor Udo.Metrics.WorkerSupervisor

  def start_link(_opts) do
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @impl true
  def init(:ok) do
    children = [
      {Registry, keys: :unique, name: @registry},
      {DynamicSupervisor, name: @supervisor, strategy: :one_for_one}
    ]

    Supervisor.init(children, strategy: :one_for_all)
  end

  def bump(path) when is_binary(path) do
    require Logger
    Logger.debug("[Udo.Metrics] bump(#{inspect(path)})")

    pid =
      case Registry.lookup(@registry, path) do
        [{pid, _}] ->
          Logger.debug("[Udo.Metrics] existing pid found")
          pid

        [] ->
          case DynamicSupervisor.start_child(@supervisor, {@worker, path}) do
            {:ok, pid} -> pid
            {:error, {:already_started, pid}} -> pid
          end
      end

    Logger.debug("[Udo.Metrics] selected pid: #{inspect(pid)}")

    send(pid, :bump)
  end
end
