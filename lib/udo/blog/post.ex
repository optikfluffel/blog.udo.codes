defmodule Udo.Blog.Post do
  @moduledoc false

  @derive {Phoenix.Param, key: :slug}

  @enforce_keys [
    :slug,
    :title,
    :summary,
    :tags,
    :published_at,
    :body,
    :created_at,
    :fediverse_creator
  ]
  defstruct [
    :slug,
    :title,
    :summary,
    :tags,
    :published_at,
    :updated_at,
    :body,
    :created_at,
    :fediverse_creator
  ]

  def build(file_path, attrs, body) do
    # Get the last two path segments
    [year, filename] =
      file_path
      |> Path.rootname()
      |> Path.split()
      |> Enum.take(-2)

    # Extract the date and time from the filename itself
    [month, day, slug_with_md] = String.split(filename, "-", parts: 3)

    # Convert to Date
    created_at = Date.from_iso8601!("#{year}-#{month}-#{day}")

    # Get rid of the .md file extension
    slug = Path.rootname(slug_with_md)

    struct!(__MODULE__, [slug: slug, created_at: created_at, body: body] ++ Map.to_list(attrs))
  end
end
