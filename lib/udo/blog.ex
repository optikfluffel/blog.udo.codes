defmodule Udo.Blog do
  @moduledoc """
  The Blog context.
  """

  alias Udo.Blog.Post

  use NimblePublisher,
    build: Post,
    from: Application.app_dir(:udo, "priv/posts/**/*.md"),
    as: :posts,
    highlighters: [:makeup_elixir, :makeup_erlang]

  defmodule NotFoundError do
    defexception [:message, plug_status: 404]
  end

  # only include posts that have a "draft" tag in development
  included_posts =
    case Mix.env() do
      :dev -> @posts
      _ -> Enum.filter(@posts, fn %Post{tags: tags} -> "draft" not in tags end)
    end

  # The @posts variable is first defined by NimblePublisher.
  # Let's further modify it by sorting all posts by descending published_at.
  # Then sanitize the post bodys because of raw(@post.body) in `templates/blog/show.html.eex`
  @posts included_posts
         |> Enum.sort_by(& &1.published_at, {:desc, Date})
         |> Enum.map(fn post -> %{post | body: HtmlSanitizeEx.html5(post.body)} end)

  @tags @posts |> Enum.flat_map(& &1.tags) |> Enum.uniq() |> Enum.sort()

  @doc """
  Returns the list of all existing posts.

  ## Examples

      iex> list_posts!()
      [%Post{}, ...]

  """
  def list_posts! do
    @posts
  end

  @doc """
  Returns the list of posts that include the given tag.

  Raises `NotFoundError` if none was found.

  ## Examples

      iex> list_posts_by_tag!("elixir")
      [%Post{}, ...]

      iex> list_posts_by_tag!("java")
      ** (NotFoundError)

  """
  def list_posts_by_tag!(tag) do
    case Enum.filter(list_posts!(), &(tag in &1.tags)) do
      [] -> raise NotFoundError, "posts with tag=#{tag} not found"
      posts -> posts
    end
  end

  @doc """
  Gets a single post by the given slug.

  Raises `NotFoundError` if the Post does not exist.

  ## Examples

      iex> get_post_by_slug!("hello_world")
      %Post{}

      iex> get_post_by_slug!("bielefeld")
      ** (NotFoundError)

  """
  def get_post_by_slug!(slug) do
    case Enum.find(list_posts!(), &(&1.slug == slug)) do
      %Post{} = post -> post
      nil -> raise NotFoundError, "post with slug=#{slug} not found"
    end
  end

  @doc """
  Returns the list of all existing tags.

  ## Examples

      iex> list_tags!()
      ["foo", "bar", ...]

  """
  def list_tags! do
    @tags
  end
end
