defmodule Udo.Metrics.Metric do
  @moduledoc false

  use Ecto.Schema

  @primary_key false
  schema "metrics" do
    field(:counter, :integer, default: 0)
    field(:date, :date, primary_key: true)
    field(:path, :string, primary_key: true)

    timestamps()
  end
end
