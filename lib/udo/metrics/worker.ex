defmodule Udo.Metrics.Worker do
  @moduledoc false

  use GenServer, restart: :temporary

  import Ecto.Query, warn: false

  alias Udo.Metrics.Metric
  alias Udo.Repo

  @registry Udo.Metrics.Registry

  def start_link(path) do
    GenServer.start_link(__MODULE__, path, name: {:via, Registry, {@registry, path}})
  end

  @impl true
  def init(path) do
    Process.flag(:trap_exit, true)
    {:ok, {path, _counter = 0}}
  end

  @impl true
  def handle_info(:bump, {path, 0}) do
    Process.send_after(self(), :upsert, Enum.random(10..20) * 1_000)
    {:noreply, {path, 1}}
  end

  @impl true
  def handle_info(:bump, {path, counter}) do
    {:noreply, {path, counter + 1}}
  end

  @impl true
  def handle_info(:upsert, {path, counter}) do
    upsert!(path, counter)
    {:noreply, {path, 0}}
  end

  @impl true
  def terminate(_, {_path, 0}), do: :ok
  def terminate(_, {path, counter}), do: upsert!(path, counter)

  # Private Helper

  defp upsert!(path, counter) do
    maybe_new_metric = %Metric{
      counter: counter,
      date: Date.utc_today(),
      path: path
    }

    update_query = from(m in Metric, update: [inc: [counter: ^counter]])

    Repo.insert!(maybe_new_metric, on_conflict: update_query, conflict_target: [:date, :path])
  end
end
