defmodule Udo.Repo do
  use Ecto.Repo,
    otp_app: :udo,
    adapter: Ecto.Adapters.Postgres
end
