defmodule Udo.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    {:ok, _pid} = Logger.add_backend(Sentry.LoggerBackend)

    children = [
      # Start the Ecto repository
      Udo.Repo,
      # Start the Metrics Supervisor
      Udo.Metrics,
      # Start the Telemetry supervisor
      UdoWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Udo.PubSub},
      # Start the Endpoint (http/https)
      UdoWeb.Endpoint,
      # Start a worker by calling: Udo.Worker.start_link(arg)
      # {Udo.Worker, arg}
      {Task, &UdoWeb.Sitemap.generate/0}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Udo.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    UdoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
