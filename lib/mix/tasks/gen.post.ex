defmodule Mix.Tasks.Gen.Post do
  @shortdoc "Creates a new empty Post"

  @moduledoc """
  Generates a new `.md` file from the given title.

      mix gen.post A great, new and shiny Post!

  """

  use Mix.Task

  def run(args) do
    %{year: y, month: m, day: d} = now = NaiveDateTime.utc_now()
    title = Enum.join(args, " ")
    slug = Slug.slugify(title)
    directory = Integer.to_string(y)
    file_name = "#{format_integer(m)}-#{format_integer(d)}-#{slug}.md"
    content = empty_post_content(title, now)

    directory_path = "priv/posts/#{directory}/"

    # ensure a directory for the current year exists
    :ok =
      Application.app_dir(:udo, directory_path)
      |> Path.relative_to_cwd()
      |> File.mkdir_p!()

    path = Application.app_dir(:udo, "#{directory_path}/#{file_name}")

    :ok =
      path
      |> Path.relative_to_cwd()
      |> File.write!(content)

    Mix.shell().info("""
    Open up your new post at:
    #{path}
    """)
  end

  defp format_integer(i) do
    i
    |> Integer.to_string()
    |> String.pad_leading(2, "0")
  end

  defp empty_post_content(title, now) do
    """
    %{
      title: "#{title}",
      summary: "TODO: Summarise your post, this is used in the RSS feed, website description, previews, etc.",
      tags: ~w(draft),
      published_at: ~N[#{%{now | microsecond: {0, 0}}}],
      fediverse_creator: "@fluffel@chaos.social"
    }
    ---
    TODO: YOUR CONTENT HERE.
    """
  end
end
