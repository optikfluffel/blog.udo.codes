defmodule UdoWeb.LiveDashboard.AnalyticsPage do
  @moduledoc false
  use Phoenix.LiveDashboard.PageBuilder

  alias Udo.Metrics.Metric
  alias Udo.Repo

  @impl true
  def menu_link(_, _) do
    {:ok, "Analytics"}
  end

  @impl true
  def render(assigns) do
    ~H"""
    <.live_table
      id="analytics-table"
      dom_id="analytics-table"
      page={@page}
      title="Analytics"
      row_fetcher={&fetch_metrics/2}
      row_attrs={&row_attrs/1}
      rows_name="tables"
      default_sort_by="date"
    >
      <:col field={:path} sortable={:asc} />
      <:col :let={analytics} field={:date} sortable={:desc}>
        {Date.to_iso8601(analytics.date)}
      </:col>
      <:col field={:counter} sortable={:desc} />
    </.live_table>
    """
  end

  defp fetch_metrics(params, _node) do
    %{search: search, sort_by: sort_by, sort_dir: sort_dir, limit: limit} = params

    metrics =
      Metric
      |> Repo.all()
      |> Enum.filter(fn %Metric{path: path} ->
        search == nil or String.contains?(path, search)
      end)
      |> Enum.map(fn %Metric{path: path, date: date, counter: counter} ->
        %{path: path, date: date, counter: counter}
      end)
      |> Enum.sort_by(&Map.get(&1, sort_by), sort_dir)

    {Enum.take(metrics, limit), length(metrics)}
  end

  defp row_attrs(table) do
    [
      {"phx-click", "show_info"},
      {"phx-value-info", table[:path]},
      {"phx-page-loading", true}
    ]
  end
end
