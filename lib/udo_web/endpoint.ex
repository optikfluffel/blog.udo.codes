defmodule UdoWeb.Endpoint do
  use Sentry.PlugCapture

  use Phoenix.Endpoint, otp_app: :udo

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  @session_options [
    store: :cookie,
    key: "_udo_key",
    signing_salt: "2DCD3JLw"
  ]

  socket "/socket", UdoWeb.UserSocket,
    websocket: true,
    longpoll: false

  socket "/live", Phoenix.LiveView.Socket, websocket: [connect_info: [session: @session_options]]

  @icon_files [
    "android-chrome-192x192.png",
    "android-chrome-512x512.png",
    "apple-touch-icon-120x120-precomposed.png",
    "apple-touch-icon-120x120.png",
    "apple-touch-icon-152x152-precomposed.png",
    "apple-touch-icon-152x152.png",
    "apple-touch-icon-180x180-precomposed.png",
    "apple-touch-icon-180x180.png",
    "apple-touch-icon-60x60-precomposed.png",
    "apple-touch-icon-60x60.png",
    "apple-touch-icon-76x76-precomposed.png",
    "apple-touch-icon-76x76.png",
    "apple-touch-icon-precomposed.png",
    "apple-touch-icon.png",
    "browserconfig.xml",
    "favicon-16x16.png",
    "favicon-32x32.png",
    "favicon.ico",
    "mstile-310x310.png",
    "safari-pinned-tab.svg"
  ]

  @sitemap_files [
    "sitemap.xml",
    "sitemap-00001.xml"
  ]

  @manifest ["site.webmanifest"]

  @allowed_files Enum.concat([
                   ~w(assets fonts images robots.txt),
                   @manifest,
                   @sitemap_files,
                   @icon_files
                 ])

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :udo,
    gzip: true,
    only: @allowed_files

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Phoenix.LiveDashboard.RequestLogger,
    param_key: "request_logger",
    cookie_key: "request_logger"

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Sentry.PlugContext

  plug Plug.MethodOverride
  plug Plug.Head
  plug Plug.Session, @session_options
  plug UdoWeb.Router
end
