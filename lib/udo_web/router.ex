defmodule UdoWeb.Router do
  use UdoWeb, :router

  csp =
    case Mix.env() do
      :dev ->
        "default-src 'self'; script-src 'self' 'unsafe-eval' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; font-src data:; base-uri 'self'; img-src 'self' data:; connect-src ws://localhost:4000"

      _ ->
        "default-src 'none'; script-src 'self'; style-src 'self'; base-uri 'self'; img-src 'self'; form-action 'none'; frame-ancestors 'none'"
    end

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:put_root_layout, {UdoWeb.Layouts, :root})
    plug(:protect_from_forgery)

    plug(:put_secure_browser_headers, %{
      "content-security-policy" => csp,
      "permissions-policy" => "interest-cohort=()",
      "referrer-policy" => "no-referrer"
    })

    plug(UdoWeb.Plugs.BumpMetric)
  end

  pipeline :rss_feed do
    plug(:accepts, ["rss", "xml"])
    plug(UdoWeb.Plugs.SendIfNoneMatch)
    plug(UdoWeb.Plugs.BumpMetric)
  end

  pipeline :json_feed do
    plug(:accepts, ["json"])
    plug(UdoWeb.Plugs.SendIfNoneMatch)
    plug(UdoWeb.Plugs.BumpMetric)
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() == :dev do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through(:browser)

      live_dashboard("/dashboard",
        metrics: UdoWeb.Telemetry,
        ecto_repos: [Udo.Repo],
        additional_pages: [analytics: UdoWeb.LiveDashboard.AnalyticsPage]
      )
    end
  end

  scope "/", UdoWeb do
    pipe_through(:rss_feed)

    get("/feed.rss", FeedController, :index_rss)
  end

  scope "/", UdoWeb do
    pipe_through(:json_feed)

    get("/feed.json", FeedController, :index_json)
  end

  scope "/", UdoWeb do
    pipe_through(:browser)

    resources("/tags", TagController, only: [:index, :show], param: "slug")

    resources("/", BlogController, only: [:index, :show], param: "slug")
  end
end
