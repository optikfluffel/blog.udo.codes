defmodule UdoWeb.Plugs.BumpMetric do
  @moduledoc """
  Calls `Udo.Metrics.bump/1` on requests
  that are about to be send with a status code of `200`,
  but only when there's no `"dashboard"` in it's path.
  """

  import Plug.Conn

  def init(default), do: default

  def call(conn, _opts) do
    register_before_send(conn, &maybe_bump_metric/1)
  end

  defp maybe_bump_metric(%Plug.Conn{status: 200, path_info: path_info} = conn) do
    if not Enum.member?(path_info, "dashboard") do
      path = "/" <> Enum.join(conn.path_info, "/")
      Udo.Metrics.bump(path)
    end

    conn
  end

  defp maybe_bump_metric(%Plug.Conn{} = conn), do: conn
end
