defmodule UdoWeb.Plugs.SendIfNoneMatch do
  @moduledoc """
  Plug for handling `"if-none-match"` request headers.

  Either sends an empty response with status `304`,
  or adds an `etag` header to the response,
  depending on the incoming `"if-none-match"` request header.
  """

  import Plug.Conn

  def init(opts), do: opts

  def call(%Plug.Conn{resp_body: ""} = conn, _opts), do: conn

  def call(%Plug.Conn{} = conn, _opts) do
    register_before_send(conn, fn %Plug.Conn{resp_body: body} = conn ->
      etag_value = body |> :erlang.phash2() |> Integer.to_string(16)
      weak_etag = "W/\"" <> etag_value <> "\""

      if weak_etag in get_req_header(conn, "if-none-match") do
        conn |> send_resp(304, "") |> halt()
      else
        put_resp_header(conn, "etag", weak_etag)
      end
    end)
  end
end
