defmodule UdoWeb.Sitemap do
  @moduledoc """
  Generates a sitemap containing all Post urls.
  """
  use UdoWeb, :controller

  alias Udo.Blog
  alias Udo.Blog.Post
  # alias UdoWeb.Endpoint
  # alias UdoWeb.Router.Helpers, as: Routes

  def generate do
    config = [
      store: Sitemapper.FileStore,
      store_config: [path: Application.get_env(:udo, :static_dir, "priv/static")],
      sitemap_url: url(~p"/")
    ]

    sitemap_url_stream = Blog.list_posts!() |> Stream.map(&post_to_url/1)

    with_gzip = Sitemapper.generate(sitemap_url_stream, config)
    without_gzip = Sitemapper.generate(sitemap_url_stream, [{:gzip, false} | config])

    :ok =
      with_gzip
      |> Stream.concat(without_gzip)
      |> Sitemapper.persist(config)
      |> Stream.run()
  end

  defp post_to_url(%Post{published_at: published_at, updated_at: updated_at} = post) do
    %Sitemapper.URL{
      loc: url(~p"/#{post}"),
      changefreq: :weekly,
      lastmod: choose_later_datetime(published_at, updated_at)
    }
  end

  defp choose_later_datetime(a, nil), do: a
  defp choose_later_datetime(nil, b), do: b

  defp choose_later_datetime(a, b) do
    case NaiveDateTime.compare(a, b) do
      :gt -> a
      :lt -> b
    end
  end
end
