defmodule UdoWeb.Layouts do
  @moduledoc false

  use UdoWeb, :html

  embed_templates "layouts/*"
end
