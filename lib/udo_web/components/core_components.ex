defmodule UdoWeb.CoreComponents do
  @moduledoc """
  Template Helpers.
  """
  use Phoenix.Component

  @doc """
  Renders a list of <meta> tags.
  """

  attr :meta_data, :list, default: []

  def meta_tags(assigns) do
    ~H"""
    <%= for meta <- @meta_data do %>
      <.meta_tag content={meta[:content]} name={meta[:name]} property={meta[:property]} />
    <% end %>
    """
  end

  @doc """
  Renders a single <meta> tag.
  """

  attr :content, :string, required: true
  attr :name, :string, default: nil
  attr :property, :string, default: nil

  def meta_tag(assigns) do
    ~H"""
    <meta content={@content} name={@name} property={@property} />
    """
  end
end
