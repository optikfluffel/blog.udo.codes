defmodule UdoWeb.TagHTML do
  @moduledoc false

  use UdoWeb, :html

  embed_templates "tag_html/*"
end
