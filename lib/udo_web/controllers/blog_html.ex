defmodule UdoWeb.BlogHTML do
  @moduledoc false

  use UdoWeb, :html

  embed_templates "blog_html/*"
end
