defmodule UdoWeb.TagController do
  use UdoWeb, :controller

  alias Udo.Blog

  def index(conn, _params) do
    tags = Blog.list_tags!()
    meta_data = [[name: "description", content: "A list of all Tags on blog.udo.codes"]]

    render(conn, "index.html", tags: tags, meta_data: meta_data, page_title: "Tags")
  end

  def show(conn, %{"slug" => slug}) do
    posts = Blog.list_posts_by_tag!(slug)

    meta_data = [
      [name: "description", content: ["All posts containing #", slug, " in it's tags."]]
    ]

    render(conn, "show.html",
      posts: posts,
      tag: slug,
      meta_data: meta_data,
      page_title: ["#", slug]
    )
  end
end
