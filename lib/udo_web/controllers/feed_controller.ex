defmodule UdoWeb.FeedController do
  use UdoWeb, :controller

  alias Udo.Blog

  @language "en"
  @title "blog.udo.codes"
  @description "My personal blog for all things code related."

  @doc """
  Generates an RSS feed for the blog posts.

  This function retrieves the list of blog posts, creates an RSS channel with the blog's metadata
  and maps each post to an RSS item.

  The resulting RSS feed is then rendered as an XML response.
  """
  def index_rss(conn, _params) do
    posts = Blog.list_posts!()

    channel = %RSS.Channel{
      language: @language,
      title: @title,
      description: @description,
      link: url(~p"/"),
      atom_link: url(~p"/feed.rss")
    }

    items = Enum.map(posts, &post_to_rss_item/1)

    {:ok, xml_content} = RSS.feed(channel, items)

    conn
    |> put_resp_header("content-type", "application/rss+xml")
    |> send_resp(200, xml_content)
  end

  @doc """
  Generates a JSON feed for the blog posts.

  This function retrieves the list of blog posts, creates a JSON feed with the blog's metadata
  and maps each post to a JSON item.

  The resulting JSON feed is then rendered as a JSON response.
  """
  def index_json(conn, _params) do
    posts = Blog.list_posts!()

    feed = %{
      language: @language,
      title: @title,
      description: @description,
      home_page_url: url(~p"/"),
      feed_url: url(~p"/feed.json"),
      items: Enum.map(posts, &post_to_json_item/1),
      version: "https://jsonfeed.org/version/1.1"
    }

    json(conn, feed)
  end

  @spec post_to_rss_item(%Blog.Post{}) :: RSS.Item.t()
  defp post_to_rss_item(%Blog.Post{} = post) do
    post_url = url(~p"/#{post}")

    %RSS.Item{
      title: post.title,
      link: post_url,
      guid: %RSS.Item.Guid{value: post_url},
      description: post.summary,
      pub_date: post.published_at
    }
  end

  @spec post_to_json_item(%Blog.Post{}) :: map()
  defp post_to_json_item(%Blog.Post{} = post) do
    post_url = url(~p"/#{post}")

    %{
      id: post_url,
      url: post_url,
      title: post.title,
      summary: post.summary,
      content_html: post.body,
      date_published: Timex.format!(post.published_at, "{ISO:Extended:Z}")
    }
  end
end
