defmodule UdoWeb.BlogController do
  use UdoWeb, :controller

  alias Udo.Blog

  def index(conn, _params) do
    posts = Blog.list_posts!()

    meta_data = [
      [name: "description", content: "Udo's personal blog for all things code related."],
      [property: "og:title", content: "blog.udo.codes"]
    ]

    render(conn, :index, posts: posts, meta_data: meta_data, shareable: true)
    # render(conn, :index, blog_posts: blog_posts)
  end

  def show(conn, %{"slug" => post_slug}) do
    post = Blog.get_post_by_slug!(post_slug)

    meta_data = [
      [name: "description", content: post.summary],
      [name: "fediverse:creator", content: post.fediverse_creator],
      [property: "og:title", content: post.title],
      [property: "og:type", content: "article"]
    ]

    render(conn, :show, post: post, page_title: post.title, meta_data: meta_data, shareable: true)
  end
end
