// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html"

// Web Share API Button
const showHiddenWrapper = wrapper => {
  wrapper.style.display = "inline"
}

const addClickEventListener = shareButton => {
  shareButton.addEventListener("click", async (event) => {
    event.preventDefault()

    const title = document.title
    const text = document.querySelector("meta[name=description]").content
    const url = document.querySelector("link[rel=canonical]")
      ? document.querySelector("link[rel=canonical]").href
      : document.location.href

    try {
      await navigator.share({ title, text, url })
      console.info("Thanks for sharing!")
    } catch (error) {
      switch (error.name) {
        case "AbortError":
          break
        default:
          console.error(err)
      }
    }
  })
}

if (navigator.share) {
  const wrappers = document.querySelectorAll("span.web-share-button")
  const shareButtons = document.querySelectorAll("span.web-share-button > a")

  wrappers.forEach(showHiddenWrapper)
  shareButtons.forEach(addClickEventListener)
}
