defmodule Udo.MixProject do
  use Mix.Project

  def project do
    [
      app: :udo,
      version: "0.11.2",
      elixir: "~> 1.11",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [summary: [threshold: 65]],
      dialyzer: [
        flags: ["-Wunmatched_returns", :error_handling, :underspecs],
        plt_add_apps: [:mix]
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Udo.Application, []},
      extra_applications: [:logger, :runtime_tools, :phoenix_ecto]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.7"},
      {:phoenix_html, "~> 4.0"},
      {:phoenix_ecto, "~> 4.4"},
      {:ecto_sql, "~> 3.9"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_live_reload, "~> 1.4", only: [:dev]},
      {:phoenix_live_dashboard, "~> 0.7"},
      {:phoenix_live_view, "~> 1.0.0", override: true},
      {:esbuild, "~> 0.7", runtime: Mix.env() == :dev},
      {:ecto_psql_extras, "~> 0.7"},
      {:telemetry_metrics, "~> 1.0"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, "~> 0.22"},
      {:jason, "~> 1.4"},
      {:bandit, "~> 1.2"},
      {:makeup_elixir, "~> 1.0"},
      {:makeup_erlang, "~> 1.0"},
      {:timex, "~> 3.7"},
      {:ex_doc, "~> 0.29"},
      {:slugify, "~> 1.3"},
      {:nimble_publisher, "~> 1.0"},
      {:html_sanitize_ex, "~> 1.4"},
      {:rss_builder, git: "https://gitlab.com/optikfluffel/rss_builder.git", branch: "use-xmerl"},
      {:sitemapper, "~> 0.7"},
      {:sentry, "~> 10.1"},
      {:credo, "~> 1.7", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.2", only: [:dev], runtime: false},
      {:mix_test_watch, "~> 1.1", only: [:dev], runtime: false},
      {:mix_audit, "~> 2.1", only: [:dev, :test], runtime: false},
      {:sobelow, "~> 0.11", only: [:dev, :test]}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      check: ["format --check-formatted", "credo --strict", "deps.audit", "sobelow", "dialyzer"],
      setup: ["deps.get"],
      "assets.deploy": ["esbuild default --minify", "phx.digest"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"]
    ]
  end
end
