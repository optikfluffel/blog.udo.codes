#! /usr/bin/bash

PROJECT_HOME="$HOME/blog.udo.codes"
LOG_FILE="$HOME/logs/deployment.log"

source "$PROJECT_HOME/.envrc"

function log() {
  local LEVEL=$1
  local MESSAGE=${@:2}
  echo "`date` - [$LEVEL] - $MESSAGE"
}

function info() {
  log "INFO" "$@"
}

function warn() {
  log "WARN" "$@"
}

function error() {
  log "ERROR" "$@"
}

function handle_git_error() {
    if [ $? -ne 0 ]; then
        error "git $1 failed"
        exit 1
    fi
}

function git_pull() {
  info "updating git repository"
  if [ ! -d "$PROJECT_HOME/.git" ]; then
    error "$PROJECT_HOME is not a git repository. clone project first."
  else
    cd "$PROJECT_HOME"
    git checkout main
    git reset --hard HEAD
    git clean -d -f
    git pull origin main
    handle_git_error "pull origin main"
  fi
}

function compile_elixir() {
  info "compiling elixir app"
  mix deps.get --only=prod
  mix compile
}

function migrate_database() {
  info "migrating postgresql database"
  mix ecto.migrate
}

function prepare_assets() {
  info "preparing assets"
  mix esbuild.install --if-missing
  mix assets.deploy
}

function release() {
  info "compiling mix release"
  mix release --overwrite
}

function restart_daemon() {
  info "restarting supervisord daemon"
  supervisorctl restart blogg-udo-codes
}

{
  git_pull
  compile_elixir
  migrate_database
  prepare_assets
  release
  restart_daemon
  info "output logged to $LOG_FILE"
} | tee "$LOG_FILE"
