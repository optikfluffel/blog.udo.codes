%{
  title: "List Post",
  summary: "A little list to round things up.",
  tags: ~w(dummy),
  published_at: ~N[2022-01-26 11:22:00],
  fediverse_creator: "@fluffel@chaos.social"
}
---
- Jujubes sugar
- Plum gummi bears
- Halvah chocolate cake
- Sesame snaps gummies
- Pumpkin spices latte
