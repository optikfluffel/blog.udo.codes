%{
  title: "A Quote",
  summary: "And a quote, just to be sure.",
  tags: ~w(test unique),
  published_at: ~N[2022-01-29 20:03:00],
  fediverse_creator: "@fluffel@chaos.social"
}
---
> Dragée icing cotton candy I love sweet roll I love gingerbread jelly beans. __Cheesecake cookie biscuit topping__ icing topping jujubes. Gingerbread chocolate chupa chups lollipop gingerbread I love pie marzipan. Wafer [sesame](https://en.wikipedia.org/wiki/Sesame){: title="Sesame - Wikipedia"} snaps jelly.
