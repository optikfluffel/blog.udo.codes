%{
  title: "Plug Example",
  summary: "A very simple example on how to use Plug.",
  tags: ~w(test elixir),
  published_at: ~N[2022-02-02 16:20:00],
  fediverse_creator: "@fluffel@chaos.social"
}
---
## A `Hello World` from Plug

```elixir
defmodule MyPlug do
  import Plug.Conn

  def init(options) do
    # initialize options
    options
  end

  def call(conn, _opts) do
    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(200, "Hello world")
  end
end
```

The snippet above shows a very simple example on how to use Plug. Save that snippet to a file and run it inside the plug application with:

```iex
iex> c "path/to/file.ex"
[MyPlug]
iex> {:ok, _} = Plug.Cowboy.http MyPlug, []
{:ok, #PID<...>}
```
