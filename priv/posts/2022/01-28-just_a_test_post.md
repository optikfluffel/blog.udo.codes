%{
  title: "Just a test post",
  summary: "Cupcake ipsum dolor sit amet chupa chups powder. I love sesame pie snaps, they're sweet.",
  tags: ~w(test dummy),
  published_at: ~N[2022-01-28 15:23:00],
  fediverse_creator: "@fluffel@chaos.social"
}
---
Dessert jelly-o marzipan chupa chups caramels macaroon _sweet roll marshmallow_ sweet. Oat cake toffee macaroon cookie halvah muffin. Jelly beans tart dragée sweet marshmallow liquorice. I love apple pie pastry. Donut cake carrot cake carrot cake gummi bears macaroon. ~~Cheesecake jelly beans brownie~~ croissant cupcake liquorice icing. __Apple pie dessert__ dragée cake. Powder gummi bears jujubes toffee ice cream ice cream ice cream chocolate chip cookie cream.
