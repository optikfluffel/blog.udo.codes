# [blog.udo.codes](https://blog.udo.codes)

Heavily inspired and copied from Dashbit's "[Welcome to our blog: how it was made!](https://dashbit.co/blog/welcome-to-our-blog-how-it-was-made)"

After reading the article, I thought having a blog again would be cool.
But after getting everything to work I remembered I don't like writing long articles that much,
so this became a (mostly) automatically deploying Phoenix application to play around with. 🤓

Some day one of the drafts on my computer will replace the example content, maybe.

## Dev Commands

```sh
# running the tests
mix test

# run formatter check, credo, dialyzer, MixAudit and sobelow
mix check

# create new post
mix gen.post My new Post

# preview server
mix phx.server
```

## Deployment

```sh
# clode repository and cd into it
git clone git@gitlab.com:optikfluffel/blog.udo.codes.git
cd blog.udo.codes

# copy example .envrc and configure to your needs
cp .envrc.example .envrc
nano .envrc
source .envrc

# get dependencies and compile
mix deps.get --only prod
mix compile

# prepare assets
mix assets.deploy

# build release and run
mix release
_build/prod/rel/udo/bin/udo start
```
