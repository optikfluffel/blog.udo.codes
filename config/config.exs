# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :udo,
  ecto_repos: [Udo.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :udo, UdoWeb.Endpoint,
  adapter: Bandit.PhoenixAdapter,
  url: [host: "localhost"],
  secret_key_base: "my4t1+hC4D5QCRNlebZUqjQ86+F4+Yv9Rx8U1xRgLcHsQ1umZbc/sKbDljWb08G3",
  render_errors: [
    formats: [html: UdoWeb.ErrorHTML],
    layout: false
  ],
  pubsub_server: Udo.PubSub,
  live_view: [signing_salt: "BC2uCWRz"]

config :esbuild,
  version: "0.25.0",
  default: [
    args: ~w(js/app.js --bundle --target=es2016 --outdir=../priv/static/assets),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :sentry, environment_name: Mix.env()

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
