import Config

# config/runtime.exs is executed for all environments, including
# during releases. It is executed after compilation and before the
# system starts, so it is typically used to load production configuration
# and secrets from environment variables or elsewhere. Do not define
# any compile-time configuration in here, as it won't be applied.
# The block below contains prod specific runtime configuration.
if config_env() == :prod do
  postgres_user =
    System.get_env("POSTGRES_USER") ||
      raise """
      environment variable POSTGRES_USER is missing.
      """

  postgres_password =
    System.get_env("POSTGRES_PASSWORD") ||
      raise """
      environment variable POSTGRES_PASSWORD is missing.
      """

  postgres_db =
    System.get_env("POSTGRES_DB") ||
      raise """
      environment variable POSTGRES_DB is missing.
      """

  postgres_socket_dir =
    System.get_env("POSTGRES_SOCKET_DIR") ||
      raise """
      environment variable POSTGRES_SOCKET_DIR is missing.
      """

  config :udo, Udo.Repo,
    username: postgres_user,
    password: postgres_password,
    database: postgres_db,
    socket_dir: postgres_socket_dir,
    pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

  secret_key_base =
    System.get_env("SECRET_KEY_BASE") ||
      raise """
      environment variable SECRET_KEY_BASE is missing.
      You can generate one by calling: mix phx.gen.secret
      """

  config :udo, UdoWeb.Endpoint,
    http: [
      # Enable IPv6 and bind on all interfaces.
      # Set it to  {0, 0, 0, 0, 0, 0, 0, 1} for local network only access.
      # See the documentation on https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html
      # for details about using IPv6 vs IPv4 and loopback vs public addresses.
      ip: {0, 0, 0, 0, 0, 0, 0, 0},
      port: String.to_integer(System.get_env("PORT") || "4000")
    ],
    secret_key_base: secret_key_base,
    server: true

  sentry_url =
    System.get_env("SENTRY_URL") ||
      raise """
      environment variable SENTRY_URL is missing.
      """

  config :sentry,
    dsn: sentry_url,
    enable_source_code_context: true,
    root_source_code_paths: [File.cwd!()],
    tags: %{
      env: "production"
    },
    included_environments: [:prod]
end
