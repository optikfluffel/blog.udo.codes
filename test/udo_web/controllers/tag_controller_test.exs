defmodule UdoWeb.TagControllerTest do
  use UdoWeb.ConnCase, async: true

  describe "index/2" do
    test "renders", %{conn: conn} do
      conn = get(conn, ~p"/tags")

      assert response = html_response(conn, 200)
      assert response =~ ~s(<div class="tag-list">)

      assert response =~
               ~s(<meta content="A list of all Tags on blog.udo.codes" name="description">)
    end
  end

  describe "show/2" do
    test "renders", %{conn: conn} do
      conn = get(conn, ~p"/tags/dummy")

      assert response = html_response(conn, 200)
      assert response =~ "All posts containing tag <strong>#dummy</strong>"

      assert response =~
               ~s(<meta content="All posts containing #dummy in it&#39;s tags." name="description">)
    end

    test "errors", %{conn: conn} do
      assert_error_sent 404, fn ->
        get(conn, "/tags/non_existing")
      end
    end
  end
end
