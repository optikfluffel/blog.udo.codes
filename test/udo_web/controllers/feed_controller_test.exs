defmodule UdoWeb.FeedControllerTest do
  use UdoWeb.ConnCase, async: true

  test "renders RSS Feed", %{conn: conn} do
    assert rss_feed =
             conn
             |> put_req_header("accept", "application/rss+xml")
             |> get("/feed.rss")
             |> response(200)

    assert String.contains?(rss_feed, "<title>blog.udo.codes</title>")
    assert String.contains?(rss_feed, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>")

    assert String.contains?(
             rss_feed,
             "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">"
           )
  end

  test "renders JSON Feed", %{conn: conn} do
    assert json_feed =
             conn
             |> put_req_header("accept", "application/json")
             |> get("/feed.json")
             |> response(200)

    assert String.contains?(json_feed, "\"title\":\"blog.udo.codes\"")
    assert String.contains?(json_feed, "\"version\":\"https://jsonfeed.org/version/1.1\"")
    assert {:ok, _decoded} = Jason.decode(json_feed)
  end
end
