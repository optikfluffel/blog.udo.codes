defmodule UdoWeb.BlogControllerTest do
  use UdoWeb.ConnCase, async: true

  describe "index/2" do
    test "renders", %{conn: conn} do
      conn = get(conn, ~p"/")

      assert response = html_response(conn, 200)
      assert response =~ "<title>\n      blog.udo.codes\n    </title>"
      assert response =~ "<dl>"
      assert response =~ "</dl>"
      assert response =~ ~s(<meta content="blog.udo.codes" property="og:title">)

      assert response =~
               ~s(<meta content="Udo&#39;s personal blog for all things code related." name="description">)
    end
  end

  describe "show/2" do
    test "renders", %{conn: conn} do
      conn = get(conn, ~p"/a_quote")

      assert response = html_response(conn, 200)
      assert response =~ "<title>\n      A Quote\n    </title>"
      assert response =~ "<h1>A Quote</h1>"
      assert response =~ ~s(<meta content="A Quote" property="og:title">)
      assert response =~ ~s(<meta content="article" property="og:type">)
    end

    test "errors", %{conn: conn} do
      assert_error_sent 404, fn ->
        get(conn, "/non_existing")
      end
    end
  end
end
