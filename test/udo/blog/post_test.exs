defmodule Udo.Blog.PostTest do
  use ExUnit.Case, async: true

  alias Udo.Blog.Post

  @file_path "/tmp/fake_posts/2020/09-17-a_fake-post.md"

  @attrs %{
    title: "A Fake-Post",
    summary: "Nothing to see here, please move along.",
    tags: ~w(fake test),
    published_at: ~N[2020-09-17 10:20:30],
    fediverse_creator: "@someone@example.tld"
  }

  @body """
  This. Is. __MARKDOWN!__
  """

  describe "build/3" do
    test "extracts slug" do
      assert %Post{slug: "a_fake-post"} = Post.build(@file_path, @attrs, @body)
    end

    test "extracts created_at" do
      assert %Post{created_at: ~D[2020-09-17]} = Post.build(@file_path, @attrs, @body)
    end

    test "adds given body" do
      assert %Post{body: post_body} = Post.build(@file_path, @attrs, @body)
      assert post_body == @body
    end

    test "adds relevant given attrs" do
      assert %Post{} = post = Post.build(@file_path, @attrs, @body)
      assert post.title == @attrs.title
      assert post.summary == @attrs.summary
      assert post.tags == @attrs.tags
      assert post.published_at == @attrs.published_at
    end
  end
end
