defmodule Udo.BlogTest do
  use ExUnit.Case, async: true

  alias Udo.Blog
  alias Udo.Blog.{NotFoundError, Post}

  test "list_posts/0" do
    assert posts = Blog.list_posts!()
    assert length(posts) > 0
    assert %Post{} = Enum.random(posts)
  end

  test "get_post_by_slug!/1" do
    assert %Post{title: "Plug Example"} = Blog.get_post_by_slug!("plug_example")

    assert_raise NotFoundError, fn ->
      Blog.get_post_by_slug!("something-bogus")
    end
  end

  test "list_posts_by_tag!/1" do
    assert [%Post{title: "Plug Example"}] = Blog.list_posts_by_tag!("elixir")

    assert_raise NotFoundError, fn ->
      Blog.list_posts_by_tag!("something-bogus")
    end
  end

  test "list_tags/0" do
    assert tags = Blog.list_tags!()
    assert length(tags) > 0
    assert Enum.member?(tags, "elixir")
  end
end
