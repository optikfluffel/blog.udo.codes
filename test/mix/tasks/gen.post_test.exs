defmodule Mix.Tasks.Gen.PostTest do
  use ExUnit.Case, async: true

  alias Mix.Tasks.Gen.Post

  Mix.shell(Mix.Shell.Process)

  test "creates an empty Post in a markdown file" do
    Post.run(["A", "generated", "Test", "Post"])

    assert_received {:mix_shell, :info, [output]}
    assert output =~ "Open up your new post at:"

    assert "Open up your new post at:\n" <> path = String.trim(output)
    assert File.exists?(path)
    assert File.read!(path) =~ "title: \"A generated Test Post\""
    assert :ok = File.rm!(path)
  end
end
